Webform Token Files
===================

This small module extends the [Webform][1] module. It allows site managers to
configure file form components to rename uploaded files using tokens. This can
help to reduce server issues caused by too many files in the same directory (by
using a current date token) or to collect related documents together (by using
a submission token, perhaps the submission ID).

[1]: https://drupal.org/project/webform

Usage
-----

Unpack the module files in an appropriate directory (`sites/all/modules/` is a
good default) and enable it in the admin interface.

Create a new, or edit an existing, *file* component in a Webform node.

Enter value in the **Rename pattern** field. You can use tokens from the
Webform node, the Webform submission and the uploaded file in your path, click
the "Browse available tokens" link in the field description to see a complete
list.

Files uploaded in this Webform component will be stored in the configured
**Upload destination** under the configured **Upload directory** with the file
and path **Rename pattern**.

Example
-------

Configure a *file* component called "CV" with:

- **Upload destination** `Private files`
- **Upload directory** `webform/applications`
- **Rename pattern** `[node:title]/application-[submission:sid]/cv/[file:basename]`

A file called `thomas-sutton.pdf` uploaded in this field will be saved in a
location like the following:

`private://webform/applications/CELTA/application-123/cv/thomas-sutton.pdf`

Where `CELTA` is the title of the Webform node; `123` is the Webform submission
ID; and `thomas-sutton.pdf` is the original name of the uploaded file.

Security
--------

**WARNING**: this module does *not* attempt to restrict the destination for
files being uploaded. You should *not* use tokens which contain un-safe,
user-supplied data to rename uploaded files. If possible, use *only* data which
is created by an administrator (e.g. Webform node titles) or generated
automatically by the system (e.g. Webform submission IDs).
